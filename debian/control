Source: live-wrapper
Maintainer: Debian Live <debian-live@lists.debian.org>
Uploaders: Iain R. Learmonth <irl@debian.org>, Steve McIntyre <93sam@debian.org>
Section: misc
Priority: optional
Build-Depends: debhelper (>= 9),
               dh-python,
               python3-all,
               python3-apt,
               python3-requests,
               python3-setuptools,
               python3-sphinx,
               vmdebootstrap (>= 1.7-1+nmu1)
Standards-Version: 4.0.0
Vcs-Browser: https://salsa.debian.org/live-team/live-wrapper
Vcs-Git: https://salsa.debian.org/live-team/live-wrapper.git
Homepage: https://debian-live.alioth.debian.org/live-wrapper/

Package: live-wrapper
Architecture: all
Depends: ${misc:Depends},
         ${python:Depends},
         debian-archive-keyring,
         isolinux,
         python3-distro-info,
         squashfs-tools,
         xorriso
Suggests: cmdtest,
          live-wrapper-doc
Description: Wrapper for vmdebootstrap for creating live images
 live-wrapper is a wrapper around vmdebootstrap to install a live Debian
 system into an ISO image, which can be used by booting from optical media or a
 USB device.
 .
 isolinux and grub2 bootloaders can be supported, depending on the distribution
 and the architecture.

Package: live-wrapper-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Wrapper for vmdebootstrap for creating live images (Documentation)
 live-wrapper is a wrapper around vmdebootstrap to install a live Debian
 system into an ISO image, which can be used by booting from optical media or a
 USB device.
 .
 isolinux and grub2 bootloaders can be supported, depending on the distribution
 and the architecture.
 .
 This package contains documentation on using live-wrapper to create live
 images.
